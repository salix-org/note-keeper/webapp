import getConfig from 'next/config';
import styles from './Header.module.css';

const { publicRuntimeConfig } = getConfig();

const Header = () => (
    <div className={styles.navbar}>
        <div className={styles.content}>
            <h1>Note Keeper</h1>
            <h3>v{publicRuntimeConfig.version}</h3>
        </div>
    </div>
);

export default Header;
