import styles from './Button.module.css';

export interface ButtonProps {
    label: string;
    type: 'button' | 'submit';
    onClick: () => void;
}

const Button = ({ label, type, onClick }: ButtonProps) => (
    <button className={styles.button} type={type} onClick={() => onClick()}>
        {label}
    </button>
);

export default Button;
