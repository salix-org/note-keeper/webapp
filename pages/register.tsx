import Button from '../components/basic/Button';
import styles from './register.module.css';

const Register = () => (
    <div className={styles.pageContainer}>
        <form action="#" method="post">
            <div className={styles.floatingContainer}>
                <input
                    className={styles.input}
                    name="email"
                    type="email"
                    required
                    placeholder="email"
                />
                <input
                    className={styles.input}
                    name="password"
                    type="password"
                    required
                    placeholder="password"
                />
                <div className={styles.buttonContainer}>
                    <Button type="submit" label="Login" onClick={() => {}} />
                    <Button type="button" label="Register" onClick={() => {}} />
                </div>
            </div>
        </form>
    </div>
);

export default Register;
