import type { NextPage } from 'next';

const Home: NextPage = () => {
    return <div>Hello index!</div>;
};

export default Home;
