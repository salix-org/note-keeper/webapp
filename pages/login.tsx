import Button from '../components/basic/Button';
import styles from './login.module.css';

const Login = () => (
    <div className={styles.pageContainer}>
        <form action="#" method="post">
            <div className={styles.floatingContainer}>
                <input
                    className={styles.input}
                    name="email"
                    type="email"
                    required
                    placeholder="email"
                />
                <input
                    className={styles.input}
                    name="password"
                    type="password"
                    required
                    placeholder="password"
                />
                <div className={styles.buttonContainer}>
                    <Button type="submit" label="Login" onClick={() => {}} />
                    <Button type="button" label="Register" onClick={() => {}} />
                </div>
            </div>
        </form>
    </div>
);

export default Login;
